#!/bin/bash
# porteus-chroot is a script that creates chroot environment of Porteus.
# This script requires AUFS support on running system.
# Use this script on your own risk.
# created by i3slkiller <https://gitlab.com/i3slkiller/porteus-chroot>

set -e

VERSION=1

unset CHROOTDIR

showHelp() {
  echo "Usage: $0 [parameters] [porteus dir*]"
  echo ""
  echo "Arguments:"
  echo "  -h, --help                  show help"
  echo "  -c, --chroot path           set chroot path (default: /tmp/chroot-RANDOM)"
  echo "  -s, --shared path           share host directory with chroot (can be used multiple times)"
  echo "  -w, --sharedrw path         share writable host directory with chroot (can be used multiple times)"
  echo "  -v, --version               show script version"
  echo ""
  echo "* - porteus dir is optional on Porteus and derivatives, and required on other distros"
}

showVersion() {
  echo "porteus-chroot version $VERSION <https://gitlab.com/i3slkiller/porteus-chroot>"
}

OPTS=hvc:s:w:
LONGOPTS=help,version,chroot:,shared:,sharedrw:

ARGS=$(getopt --options=$OPTS --longoptions=$LONGOPTS --name $0 -- $@)
[ $? != 0 ] && echo "Command arguments parsing error" && exit 126

eval set -- $ARGS

SHAREDDIRS=()
SHAREDDIRSRW=()

while true; do
  case $1 in
    -h|--help) showHelp; exit;;
    -v|--version) showVersion; exit;;
    -c|--chroot) CHROOTDIR="$2"; shift 2;;
    -s|--shared) SHAREDDIRS+=("$2"); SHAREDDIRSRW+=(0); shift 2;;
    -w|--sharedrw) SHAREDDIRS+=("$2"); SHAREDDIRSRW+=(1); shift 2;;
    --) shift; break;;
    *) echo "Unknown error"; exit 127;;
  esac
done

[ $UID != 0 ] && echo "This script need to be executed as root" && exit 1
! grep -q aufs /proc/filesystems && echo "No AUFS support detected on this system" && exit 2

if [ -n "$1" ]; then
  PORTXZMS="$1"
else
  if [ -n "$PORTDIR" ]; then
    PORTXZMS="$PORTDIR"
  else
    echo "Porteus dir must be set"
    showHelp
    exit 16
  fi
fi

showVersion

if [ -z "$CHROOTDIR" ]; then
  while true; do
    CHROOTDIR="/tmp/chroot-$RANDOM"
    [ ! -e "$CHROOTDIR" ] && break
  done
fi

mkdir -p "$CHROOTDIR"
CHROOTDIRFS=$(find "$CHROOTDIR" -mindepth 0 -maxdepth 0 -type d -printf '%F')
[ $CHROOTDIRFS == aufs ] && mount -t tmpfs -o mode=0755 porteus-chroot "$CHROOTDIR"
pushd "$CHROOTDIR"

mkdir -p live/memory/{changes,images,xino}
mkdir -p live/porteus
mkdir -p live/tmp
mkdir -p host/porteus/{base,modules,optional}
mkdir -p union

mount -t aufs -o nowarn_perm,xino=live/memory/xino/.aufs.xino,br:live/memory/changes=rw aufs union

mkdir -p union/mnt/live union/mnt/host
mount -o bind live union/mnt/live
mount -o bind host union/mnt/host

echo "Loading Porteus from \"$PORTXZMS\""
mount -o ro,bind "$PORTXZMS/base" union/mnt/host/porteus/base
[ -d "$PORTXZMS/modules" ] && mount -o ro,bind "$PORTXZMS/modules" union/mnt/host/porteus/modules && ln -sf /mnt/host/porteus/modules union/mnt/live/porteus/modules
[ -d "$PORTXZMS/optional" ] && mount -o ro,bind "$PORTXZMS/optional" union/mnt/host/porteus/optional && ln -sf /mnt/host/porteus/optional union/mnt/live/porteus/optional

find union/mnt/host/porteus/base -type f -name "*.xzm" -printf "%f\n" | sort | xargs -i bash -c '
mkdir -p "union/mnt/live/memory/images/{}"
mount -o ro "union/mnt/host/porteus/base/{}" "union/mnt/live/memory/images/{}"
mount -o remount,add=1:union/mnt/live/memory/images/{}=rr union
echo "/mnt/host/porteus/base/{}" >> union/mnt/live/tmp/modules
'

mount -t proc proc union/proc
mount -t sysfs -o ro sysfs union/sys

cp /etc/resolv.conf union/etc/

for i in etc/profile root/.bashrc home/guest/.bashrc; do
  ! grep -q "PS1='(chroot)" union/$i && sed -i "s/PS1='/PS1='(chroot) /g" union/$i
done

CHROOTDISTRO=$(find union/etc/porteu? -mindepth 0 -maxdepth 0 -type d -printf '%f')
[ $CHROOTDISTRO == porteux ] && CHROOTDISTRONAME=PorteuX || CHROOTDISTRONAME=Porteus

echo "export DISTRO=$CHROOTDISTRO
export BOOTDEV=chroot
export BASEDIR=/mnt/host
export PORTDIR=/mnt/host/porteus
export MODDIR=/mnt/host/porteus/modules
export PORTCFG=/dev/null" > union/etc/profile.d/$CHROOTDISTRO.sh
chmod 755 union/etc/profile.d/$CHROOTDISTRO.sh

echo -e "nomagic\nbase_only\nnorootcopy" > union/etc/bootcmd.cfg

echo "# Booting device:
chroot

# $CHROOTDISTRONAME data found in:
/mnt/host/porteus

# Changes are stored in:
memory

# Non standard /rootcopy dir:
none

# Modules activated during boot time:
$(cat union/mnt/live/tmp/modules)

# Base module versions:
$(cat union/etc/$CHROOTDISTRO/*)" > union/var/log/$CHROOTDISTRO-livedbg

for (( i=0; i<${#SHAREDDIRS[*]}; i++ )); do
  mkdir -p union/mnt/shared$i
  if [ ${SHAREDDIRSRW[i]} == 1 ]; then
    echo "Mounting \"${SHAREDDIRS[i]}\" to \"/mnt/shared$i\" as writable"
    mount -o bind "${SHAREDDIRS[i]}" union/mnt/shared$i
  else
    echo "Mounting \"${SHAREDDIRS[i]}\" to \"/mnt/shared$i\" as read-only"
    mount -o ro,bind "${SHAREDDIRS[i]}" union/mnt/shared$i
  fi
done

chroot union /bin/bash

popd
umount -Rl "$CHROOTDIR/union"
[ $CHROOTDIRFS == aufs ] && umount "$CHROOTDIR" && rmdir "$CHROOTDIR"
